# (c) 2018

import requests
from bs4 import BeautifulSoup
import pysftp
import configparser
import getpass
import os
import sys

config = configparser.ConfigParser()
config.read("config.ini")

try:
    url = config.get('USER', 'url')
    sftp_user = config.get('USER', 'sftp_user')
    sftp_pass = config.get('USER', 'sftp_pass')
except KeyError:
    print("Config failed to load.")
    sys.exit()

if sftp_pass == "":
    sftppass_defined = False
else:
    sftppass_defined = True

try:
    sftp_path = config.get('USER', 'path')
    sftp_host = config.get('USER', 'sftp_host')
except KeyError:
    print("Config failed to load.")
    sys.exit()

# Get file

try:
    webpage = requests.get(url)
except:
    print("Failed to get webpage, try again later")
    sys.exit()

webpage = webpage.text

# Parse file with BS4

soup = BeautifulSoup(webpage, 'html.parser')

# Ask for inputs

percentchance = input("What is the percent chance of (something): ")
chancetext = input("What type of chance is the percent for: ")
extrachance1 = input("What is extrachance1: ")
extrachance2 = input("What is extrachance2: ")
extrachance3 = input("What is extrachance3: ")
notes = input("What are the notes: ")
lastupdated = input("What's the current date/time: ")

# Start replacing text

if percentchance != "skip":
    percentchance_element = soup.find(id="percent")
    percentchance_element.string.replace_with(percentchance)

if chancetext != "skip":
    chancetext_element = soup.find(id="chancetext")
    chancetext_element.string.replace_with(chancetext)

if lastupdated != "skip":
    lastupdated_element = soup.find(id="lastupdated")
    lastupdated_element.string.replace_with("Last updated: " + lastupdated)

if extrachance1 != "skip":
    extrachance1_element = soup.find(id="extrachance1")
    extrachance1_element.string.replace_with(extrachance1)

if extrachance2 != "skip":
    extrachance2_element = soup.find(id="extrachance2")
    extrachance2_element.string.replace_with(extrachance2)

if extrachance3 != "skip":
    extrachance3_element = soup.find(id="extrachance3")
    extrachance3_element.string.replace_with(extrachance3)

if notes != "skip":
    notes_element = soup.find(id="details")
    notes_element.string.replace_with(notes)

new_html = soup.prettify()

# Create local html file
try:
    file = open('index.html', 'w')
    file.write(new_html)
    file.close()
except:
    print("Failed to create local HTML file")
    sys.exit()

# Upload via SFTP

if sftppass_defined is True:
    try:
        with pysftp.Connection(sftp_host, username=sftp_user, password=sftp_pass) as sftp:
            with sftp.cd(sftp_path):
                sftp.put('index.html')
    except:
        print("Error with uploading file to web server. Wrong password/username?")
        sys.exit()
elif sftppass_defined is False:
    print("Uploading to server %s, path: %s" % (sftp_host, sftp_path))
    password = getpass.getpass("Please enter your password: ")
    try:
        with pysftp.Connection(sftp_host, username=sftp_user, password=password) as sftp:
            with sftp.cd(sftp_path):
                sftp.put('index.html')
    except:
        print("Error with uploading file to web server. Wrong password/username?")
        sys.exit()

try:
    os.remove("index.html")
except:
    print("Failed to remove local HTML file - delete the file manually")

print("Success!")