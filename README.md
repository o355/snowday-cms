# SnowDay CMS
A CMS for managing a custom snow day predictor webpage.

# Purpose
I run a custom snow day predictor. Don't ask why.

In the early days of this snow day predictor (last year), I ran the site on WordPress. While it wasn't pretty, WordPress had one crucial thing going for it - I could edit the site on the go.

This Python script is a "replacement" for WordPress for this new snow day predictor - I can run it on my laptop & phone without touching source code.

# Usage
I'll be releasing the source code of the predictor site soon, but here's a quick usage guide.

SnowDay-CMS uses Requests for downloading source code, BeautifulSoup4 to modify some paragraphs & headers with an id, then SFTP to upload the source code back to the server.

The config file has a few options for server configs & paths - it's pretty self explainatory.

# "Flowchart"
* SnowDay-CMS makes a request to the URL specified in the config file and stores the source code.
* The user is asked for 7 inputs corresponding to 7 fields on the site
* BeautifulSoup writes to the saved copy of the source code with the new fields
* Upload via SFTP back to server

# Config File
Make a config file that looks like this:

```
[USER]
url = 
sftp_user = 
sftp_pass =
sftp_host = 
path = 
```

# Todo
Make a clean config.ini file for Git

Add error catching everywhere

Ability to skip fields and preserve the previous entry